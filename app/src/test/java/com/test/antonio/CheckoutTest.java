package com.test.antonio;

import com.test.antonio.offer.BuyOneGetOneFreeAppleOffer;
import com.test.antonio.offer.BuyThreeGetOneFreeOrangeOffer;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static com.test.antonio.model.Product.APPLE;
import static com.test.antonio.model.Product.ORANGE;
import static org.junit.Assert.assertEquals;

public class CheckoutTest {

    private Checkout checkout;

    @Test
    public void checkoutWithoutOffers() {
        checkout = new Checkout(
                Arrays.asList(APPLE, APPLE, ORANGE, APPLE),
                Collections.emptyList()
        );
        assertEquals(checkout.checkout(), new BigDecimal("2.05"));
    }

    @Test
    public void checkWithOffers() {
        checkout = new Checkout(
                Arrays.asList(APPLE, APPLE, ORANGE, APPLE, ORANGE, ORANGE, APPLE),
                Arrays.asList(
                        new BuyOneGetOneFreeAppleOffer(),
                        new BuyThreeGetOneFreeOrangeOffer()
                )
        );
        assertEquals(checkout.checkout(), new BigDecimal("1.70"));
    }
}
