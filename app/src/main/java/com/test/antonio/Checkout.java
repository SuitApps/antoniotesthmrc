package com.test.antonio;

import com.test.antonio.model.Product;
import com.test.antonio.offer.Offer;

import java.math.BigDecimal;
import java.util.List;

public class Checkout {

    private final List<Product> shoppingList;
    private final List<Offer> offers;

    public Checkout(List<Product> shoppingList, List<Offer> offers) {
        this.shoppingList = shoppingList;
        this.offers = offers;
    }

    public BigDecimal checkout() {
        BigDecimal totalPriceWithoutDiscounts = shoppingList.stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return offers.stream()
                .map(offer -> offer.offerReduction(shoppingList))
                .reduce(totalPriceWithoutDiscounts, BigDecimal::subtract);
    }
}

