package com.test.antonio.offer;

import com.test.antonio.model.Product;

import java.math.BigDecimal;
import java.util.List;

public interface Offer {

    BigDecimal offerReduction(List<Product> products);
}
