package com.test.antonio.offer;

import com.test.antonio.model.Product;

import java.math.BigDecimal;
import java.util.List;


public class BuyThreeGetOneFreeOrangeOffer implements Offer {

    @Override
    public BigDecimal offerReduction(List<Product> products) {
        long orangeItems = products.stream()
                .filter(product -> product.equals(Product.ORANGE))
                .count();
        return Product.ORANGE.getPrice()
                .multiply(BigDecimal.valueOf(orangeItems / 3));
    }
}
