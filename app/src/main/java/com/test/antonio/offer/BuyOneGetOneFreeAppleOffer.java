package com.test.antonio.offer;

import com.test.antonio.model.Product;

import java.math.BigDecimal;
import java.util.List;

public class BuyOneGetOneFreeAppleOffer implements Offer {

    @Override
    public BigDecimal offerReduction(List<Product> products) {
        long appleItems = products.stream()
                .filter(product -> product.equals(Product.APPLE))
                .count();
        return Product.APPLE.getPrice()
                .multiply(BigDecimal.valueOf(appleItems / 2));
    }
}
