package com.test.antonio.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Product {

    public static final Product APPLE = new Product(new BigDecimal("0.60"));
    public static final Product ORANGE = new Product(new BigDecimal("0.25"));

    private final BigDecimal price;

    public Product(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price);
    }
}
